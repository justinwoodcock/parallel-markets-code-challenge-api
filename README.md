# Parallel Markets Code Challenge API

This project was bootstrapped with [Strapi's npx script](https://strapi.io/documentation/v3.x/getting-started/quick-start.html).

## Available Scripts

In the project directory, you can run:

### `yarn develop`

Runs the development server and will restart the server after changes are made.

### `yarn start`

Runs in production mode

### `yarn build`

Builds the assets for deployment
