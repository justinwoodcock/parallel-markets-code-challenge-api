module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', 'mysql-10662-0.cloudclusters.net'),
        port: env.int('DATABASE_PORT', 10662),
        database: env('DATABASE_NAME', 'parallelmarkets'),
        username: env('DATABASE_USERNAME', 'strapi'),
        password: env('DATABASE_PASSWORD', '6RVimgHCo68RXzy'),
        ssl: env.bool('DATABASE_SSL', false),
      },
      options: {}
    },
  },
});
